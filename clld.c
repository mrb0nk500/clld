/*
 * Main Program file.
 *
 * Platform: 	Anything with a C compiler.
 * Author:	mr b0nk 500 <b0nk@mrb0nk500.xyz>
 * Notes: 	Who would even use this kind of collision checker?
 * Created:	September 27, 2019 @ 09:25PM
 */

#include "clld.h"
#include <stdint.h>
#ifdef DEBUG
#include <stdio.h>
#endif

uint8_t get_coldir(uint8_t flt, uint8_t col_type, int hmpeak, struct colis *cursor, struct colis *floor) {
	uint8_t coldir = 0;

	/* Get the direction of a collision. */
	uint8_t isleft = 0;
	uint8_t isright = 0;
	uint8_t istop = 0;
	uint8_t isbottom = 0;

	uint8_t isinfloor_x = 0;
	uint8_t isinfloor_y = 0;
	uint8_t isinfloor_left   = 0;
	uint8_t isinfloor_right  = 0;
	uint8_t isinfloor_top    = 0;
	uint8_t isinfloor_bottom = 0;
	switch (col_type) {
		case AABB:
			if (flt) {
				isleft   = cursor->c2 > floor->c1-cursor->xvel-1;
				isright  = cursor->c1 < floor->c2-cursor->xvel+1;
				istop    = cursor->d2 > floor->d1-cursor->yvel-1;
				isbottom = cursor->d1 < floor->d2-cursor->yvel+1;

				isinfloor_left   = cursor->d2 > floor->d1;
				isinfloor_right  = cursor->d1 < floor->d2;
				isinfloor_top    = cursor->c2 > floor->c1;
				isinfloor_bottom = cursor->c1 < floor->c2;
			} else {
				isleft   = cursor->a2 > floor->a1-cursor->xvel-1;
				isright  = cursor->a1 < floor->a2-cursor->xvel+1;
				istop    = cursor->b2 > floor->b1-cursor->yvel-1;
				isbottom = cursor->b1 < floor->b2-cursor->yvel+1;

				isinfloor_left   = cursor->b2 > floor->b1;
				isinfloor_right  = cursor->b1 < floor->b2;
				isinfloor_top    = cursor->a2 > floor->a1;
				isinfloor_bottom = cursor->a1 < floor->a2;
			}
			isinfloor_x = isinfloor_left && isinfloor_right ;
			isinfloor_y = isinfloor_top  && isinfloor_bottom;
			break;
		case THM:
			if (flt) {
				/* Did the cursor collide with the floor from the left? */
				isleft   = cursor->c2 > floor->c1-cursor->xvel-1;
				/* Did the cursor collide with the floor from the right? */
				isright  = cursor->c1 < (floor->c2-cursor->w)-cursor->xvel+1;

				/* Is the floor's heightmap vertically flipped? */
				if (!floor->flp) {
					/* Did the cursor collide with the floor from the top? */
					istop    = cursor->d2 > (floor->d2-hmpeak)-cursor->yvel;
					/* Did the cursor collide with the floor from the bottom? */
					isbottom = cursor->d1 < floor->d2-cursor->yvel+1;

					/* Is the bottom of the cursor below the floor's leftmost height? */
					isinfloor_left   = floor->d2-cursor->d2 < floor->heightmap[0] && cursor->xvel > 0;
					/* Is the bottom of the cursor below the floor's rightmost height? */
					isinfloor_right  = floor->d2-cursor->d2 < floor->heightmap[floor->w-1] && cursor->xvel < 0;
					/* Is the bottom of the cursor at, or above the floor's ground? */
					isinfloor_top    = floor->d2-cursor->d2 >= 0;
					/* Is the top of the cursor at, or below the floor's ground? */
					isinfloor_bottom = floor->d2-cursor->d1 <= 0;
				} else {
					/* Did the cursor collide with the floor from the top? */
					istop    = cursor->d2 > floor->d1-cursor->yvel-1;
					/* Did the cursor collide with the floor from the bottom? */
					isbottom = cursor->d1 < (floor->d1+hmpeak)-cursor->yvel;

					/* Is the bottom of the cursor below the floor's leftmost height? */
					isinfloor_left   = floor->d1-cursor->d1 > -floor->heightmap[0] && cursor->xvel > 0;
					/* Is the bottom of the cursor below the floor's rightmost height? */
					isinfloor_right  = floor->d1-cursor->d1 > -floor->heightmap[floor->w-1] && cursor->xvel < 0;
					/* Is the bottom of the cursor at, or above the floor's height? */
					isinfloor_top    = floor->d1-cursor->d2 >= 0 && cursor->yvel > 0;
					/* Is the top of the cursor at, or above the floor's height? */
					isinfloor_bottom = cursor->d1-floor->d1 >= 0;
				}
			} else {
				/* Did the cursor collide with the floor from the left? */
				isleft   = cursor->a2 > floor->a1-cursor->xvel-1;
				/* Did the cursor collide with the floor from the right? */
				isright  = cursor->a1 < (floor->a2-cursor->w)-cursor->xvel+1;

				/* Is the floor's heightmap vertically flipped? */
				if (!floor->flp) {
					/* Did the cursor collide with the floor from the top? */
					istop    = cursor->b2 > (floor->b2-hmpeak)-cursor->yvel;
					/* Did the cursor collide with the floor from the bottom? */
					isbottom = cursor->b1 < floor->b2-cursor->yvel+1;

					/* Is the bottom of the cursor below the floor's leftmost height? */
					isinfloor_left  = floor->b2-cursor->b2 < floor->heightmap[0];
					/* Is the bottom of the cursor below the floor's rightmost height? */
					isinfloor_right = floor->b2-cursor->b2 < floor->heightmap[floor->w-1];
					/* Is the bottom of the cursor at, or above the floor's ground? */
					isinfloor_top = floor->b2-cursor->b2 >= 0;
					/* Is the top of the cursor at, or below the floor's ground? */
					isinfloor_bottom = floor->b2-cursor->b1 <= 0;
				} else {
					/* Did the cursor collide with the floor from the top? */
					istop    = cursor->b2 > floor->b1-cursor->yvel-1;
					/* Did the cursor collide with the floor from the bottom? */
					isbottom = cursor->b1 < (floor->b1+hmpeak)-cursor->yvel;

					/* Is the bottom of the cursor below the floor's leftmost height? */
					isinfloor_left   = floor->b1-cursor->b1 > -floor->heightmap[0];
					/* Is the bottom of the cursor below the floor's rightmost height? */
					isinfloor_right  = floor->b1-cursor->b1 > -floor->heightmap[floor->w-1];
					/* Is the bottom of the cursor at, or above the floor's ground? */
					isinfloor_top    = floor->b1-cursor->b2 >= 0 && cursor->yvel > 0;
					/* Is the top of the cursor at, or below the floor's ground? */
					isinfloor_bottom = cursor->b1-floor->b1 >= 0;
				}
			}
			isinfloor_x = isinfloor_left ^ isinfloor_right ;
			isinfloor_y = isinfloor_top  ^ isinfloor_bottom;
			break;
	}
	if (isinfloor_x) {
		coldir |= isleft   << 0;
		coldir |= isright  << 1;
	}
	if (isinfloor_y) {
		coldir |= istop    << 2;
		coldir |= isbottom << 3;
	}
	return coldir;
}

/* Tile Heightmap Collision. */
void thm(int hmpeak, struct colis *cursor, struct colis *floor) {
	int newy = 0;
	uint8_t coldir    = get_coldir(0, THM , hmpeak, cursor, floor);
	uint8_t isinfloor = get_coldir(0, AABB, hmpeak, cursor, floor);
	cursor->clx = (isinfloor &  3) ==  3;
	cursor->cly = (isinfloor & 12) == 12;
	if (cursor->clx && (cursor->a2-floor->a1 < 0 || cursor->a2-floor->a1 > floor->w)) {
		switch (coldir & 3) {
			case 1: cursor->pos.x = (floor->pos.x-cursor->xvel)-cursor->w-1; break;
			case 2: cursor->pos.x = (floor->a2-cursor->w)-cursor->xvel+1; break;
		}
		cursor->xvel = 0;
		floor->xvel = 0;
	}
	if (cursor->cly && cursor->a2-floor->a1 < floor->w) {
		newy = floor->heightmap[cursor->a2-floor->a1];
		switch (coldir & 12) {
			case 4:
				if (!floor->flp) {
					cursor->pos.y = ((floor->b2-cursor->h)-newy)-cursor->yvel;
				} else {
					cursor->pos.y = (floor->pos.y-cursor->h)-cursor->yvel;
				}
				break;
			case 8:
				if (!floor->flp) {
					cursor->pos.y = (floor->pos.y+floor->h)-cursor->yvel+1;
				} else {
					cursor->pos.y = (floor->pos.y+newy)-cursor->yvel;
				}
				break;
		}
		cursor->yvel = 0;
		floor->yvel = 0;
	}
	cursor->gnded = (cursor->pos.y == ((floor->b2-cursor->h)-newy)-cursor->yvel);
	#ifdef DEBUG
	printf("%u, %u\n%u\n%i\n%i, %i\n%07.06f, %07.06f\n", cursor->clx, cursor->cly, cursor->gnded, newy, cursor->pos.x, cursor->pos.y, cursor->xvel, cursor->yvel);
	printf("%u, %u\n%u\n%i\n%i, %i\n%07.06f, %07.06f\n", floor->clx, floor->cly, floor->gnded, newy, floor->pos.x, floor->pos.y, floor->xvel, floor->yvel);
	#endif
}

/* Floating point Tile Heightmap Collision. */
void fthm(int hmpeak, struct colis *cursor, struct colis *floor) {
	int newy = 0;
	uint8_t coldir    = get_coldir(0, THM , hmpeak, cursor, floor);
	uint8_t isinfloor = get_coldir(0, AABB, hmpeak, cursor, floor);
	cursor->clx = (isinfloor &  3) ==  3;
	cursor->cly = (isinfloor & 12) == 12;

	if (cursor->clx && cursor->c2-floor->c1 < -1 || cursor->c2-floor->c1 > floor->w) {
		switch (coldir & 3) {
			case 1: cursor->pos.fx = (floor->pos.fx-cursor->xvel)-cursor->w-1; break;
			case 2: cursor->pos.fx = (floor->c2-cursor->w)-cursor->xvel+1; break;
		}
		cursor->xvel = 0;
		cursor->xvel = 0;
	}
	if (cursor->cly && cursor->c2-floor->c1 < floor->w) {
		newy = floor->heightmap[(int)cursor->c2-(int)floor->c1];
		switch (coldir & 12) {
			case 4:
				if (!floor->flp) {
					cursor->pos.fy = ((floor->d2-cursor->h)-newy)-cursor->yvel;
				} else {
					cursor->pos.fy = (floor->pos.fy-cursor->h)-cursor->yvel;
				}
				break;
			case 8:
				if (!floor->flp) {
					cursor->pos.fy = (floor->pos.fy+floor->h)-cursor->yvel;
				} else {
					cursor->pos.fy = (floor->pos.fy+newy)-cursor->yvel;
				}
				break;
		}
		cursor->yvel = 0;
		floor->yvel = 0;
	}
	cursor->gnded = (cursor->pos.fy == ((floor->d2-cursor->h)-newy)-cursor->yvel);
	#ifdef DEBUG
	printf("%u, %u\n%u\n%i\n%07.06f, %07.06f\n%07.06f, %07.06f\n", cursor->clx, cursor->cly, cursor->gnded, newy, cursor->pos.fx, cursor->pos.fy, cursor->xvel, cursor->yvel);
	printf("%u, %u\n%u\n%i\n%07.06f, %07.06f\n%07.06f, %07.06f\n", floor->clx, floor->cly, floor->gnded, newy, floor->pos.fx, floor->pos.fy, floor->xvel, floor->yvel);
	#endif
}

/* Axis Aligned Bounding Box Collision. */
void aabb(struct colis *cursor, struct colis *floor) {
	uint8_t coldir = get_coldir(0, AABB, -1, cursor, floor);
	uint8_t left   = (cursor->xvel > 0) && (!floor->xvel || floor->xvel != 0);
	uint8_t right  = (cursor->xvel < 0) && (!floor->xvel || floor->xvel != 0);
	uint8_t top    = (cursor->yvel > 0) && (!floor->yvel || floor->yvel != 0);
	uint8_t bottom = (cursor->yvel < 0) && (!floor->yvel || floor->yvel != 0);

	cursor->clx = (coldir & 0x03) == 0x03;
	cursor->cly = (coldir & 0x0C) == 0x0C;
	/* Is the Cursor is on the ground? */
	cursor->gnded = (cursor->b2 == floor->b1-cursor->yvel-1);
	if (cursor->clx) {
		if (left) {
			cursor->pos.x = (floor->pos.x-cursor->xvel)-cursor->w-1;
		} else if (!left && floor->xvel != 0) {
			if (!right && floor->xvel < 0) {
				cursor->pos.x = (floor->pos.x-cursor->xvel)-cursor->w-1;
			}
		}
		if (right) {
			cursor->pos.x = (floor->pos.x+floor->w)-cursor->xvel+1;
		} else if (!right && floor->xvel != 0) {
			if (!left && floor->xvel > 0) {
				cursor->pos.x = (floor->pos.x+floor->w)-cursor->xvel+1;
			}
		}
		cursor->xvel = 0;
		floor->xvel = 0;
	}
	if (cursor->cly) {
		if (top) {
			cursor->pos.y = (floor->pos.y-cursor->yvel)-cursor->h-1;
		} else if (!top && floor->yvel != 0) {
			if (!bottom && floor->yvel < 0) {
				cursor->pos.y = (floor->pos.y-cursor->yvel)-cursor->h-1;
			}
		}
		if (bottom) {
			cursor->pos.y = (floor->pos.y+floor->h)-cursor->yvel+1;
		} else if (!bottom && floor->yvel != 0) {
			if (!top && floor->yvel > 0) {
				cursor->pos.y = (floor->pos.y+floor->h)-cursor->yvel+1;
			}
		}
		cursor->yvel = 0;
		floor->yvel = 0;
	}
	#ifdef DEBUG
	printf("%u, %u\n%u\n%i, %i\n%07.06f, %07.06f\n", cursor->clx, cursor->cly, cursor->gnded, cursor->pos.x, cursor->pos.y, cursor->xvel, cursor->yvel);
	printf("%u, %u\n%u\n%i, %i\n%07.06f, %07.06f\n", floor->clx, floor->cly, floor->gnded, floor->pos.x, floor->pos.y, floor->xvel, floor->yvel);
	#endif
}

/* Floating point Axis Aligned Bounding Box Collision. */
void faabb(struct colis *cursor, struct colis *floor) {
	uint8_t coldir = get_coldir(1, AABB, -1, cursor, floor);
	uint8_t left   = (cursor->xvel > 0) && (!floor->xvel || floor->xvel != 0);
	uint8_t right  = (cursor->xvel < 0) && (!floor->xvel || floor->xvel != 0);
	uint8_t top    = (cursor->yvel > 0) && (!floor->yvel || floor->yvel != 0);
	uint8_t bottom = (cursor->yvel < 0) && (!floor->yvel || floor->yvel != 0);

	cursor->clx = (coldir & 0x03) == 0x03;
	cursor->cly = (coldir & 0x0C) == 0x0C;
	/* Is the Cursor is on the ground? */
	cursor->gnded = (cursor->d2 == floor->d1-cursor->yvel-1);
	if (cursor->clx) {
		if (left) {
			cursor->pos.fx = (floor->pos.fx-cursor->xvel)-cursor->w-1;
		} else if (!left && floor->xvel != 0) {
			if (!right && floor->xvel < 0) {
				cursor->pos.fx = (floor->pos.fx-cursor->xvel)-cursor->w-1;
			}
		}
		if (right) {
			cursor->pos.fx = (floor->pos.fx+floor->w)-cursor->xvel+1;
		} else if (!right && floor->xvel != 0) {
			if (!left && floor->xvel > 0) {
				cursor->pos.fx = (floor->pos.fx+floor->w)-cursor->xvel+1;
			}
		}
		cursor->xvel = 0;
		floor->xvel = 0;
	}
	if (cursor->cly) {
		if (top) {
			cursor->pos.fy = (floor->pos.fy-cursor->yvel)-cursor->h-1;
		} else if (!top && floor->yvel != 0) {
			if (!bottom && floor->yvel < 0) {
				cursor->pos.fy = (floor->pos.fy-cursor->yvel)-cursor->h-1;
			}
		}
		if (bottom) {
			cursor->pos.fy = (floor->pos.fy+floor->h)-cursor->yvel+1;
		} else if (!bottom && floor->yvel != 0) {
			if (!top && floor->yvel > 0) {
				cursor->pos.fy = (floor->pos.fy+floor->h)-cursor->yvel+1;
			}
		}
		cursor->yvel = 0;
		floor->yvel = 0;
	}
	#ifdef DEBUG
	printf("%u, %u\n%u\n%07.06f, %07.06f\n%07.06f, %07.06f\n", cursor->clx, cursor->cly, cursor->gnded, cursor->pos.fx, cursor->pos.fy, cursor->xvel, cursor->yvel);
	printf("%u, %u\n%u\n%07.06f, %07.06f\n%07.06f, %07.06f\n", floor->clx, floor->cly, floor->gnded, floor->pos.fx, floor->pos.fy, floor->xvel, floor->yvel);
	#endif
}
