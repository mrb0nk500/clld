#!/bin/sh
x1=234
y1=234
xvel=1
xspd=$xvel
yvel=1
yspd=$yvel
x2=256
y2=256
w1=16
h1=16
w2=64
h2=64
u=0
d=0
l=0
r=0
clear
actkbd -Ps | \
while read frame key held; do
	yspd=0 && xspd=0 && negx="" && negy="";
        [ "${key#*KEY_W}" != "$key" -o "${key#*KEY_S}" != "$key" ] && yspd=$yvel
        [ "${key#*KEY_A}" != "$key" -o "${key#*KEY_D}" != "$key" ] && xspd=$xvel
        [ "${key#*KEY_W}" != "$key" ] && negy="-" && y1=$(echo "$y1-$yspd" | bc)
        [ "${key#*KEY_S}" != "$key" ] && negy=""  && y1=$(echo "$y1+$yspd" | bc)
        [ "${key#*KEY_A}" != "$key" ] && negx="-" && x1=$(echo "$x1-$xspd" | bc)
        [ "${key#*KEY_D}" != "$key" ] && negx=""  && x1=$(echo "$x1+$xspd" | bc)
	xy=$(printf "1\n$w1,$h1\n$x1,$y1\n$negx$xspd,$negy$yspd\n$w2,$h2\n$x2,$y2"  | ./clld | tail -n2 | tr -d ',' | head -n1)
	x1=$(echo "$xy" | awk '{print $1}')
	y1=$(echo "$xy" | awk '{print $2}')
	printf "\33[1;1H$x1 $y1   \33[2;1H$negx$xspd $negy$yspd        \33[3;1H  \b\b"
done
