#include "clld.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#define TERASGN(a, b, c, d) (a) ? (b = d) : (c = d)

/* Collision type */
int colltype = -1;

/* Position type */
int postype = -1;

/* Level of verbosity. */
int verbose = 0;

/* Options string. */
const char *opts = "c:v:h";

static int usage() {
	printf(
		"clld Version MFIYA.\n"
		"Usage: clld [options]\n"
		"Options:\n"
		"	-c[type]	Force the collision type to be\n"
		"			the specified type (THM=0, AABB=1).\n"
		"	-v[level]	Sets the verbosity level (0-3).\n"
		"	-h		What do you fucking think it does.\n"
		"			Anyways, it brings up this cool help\n"
		"			message.\n"
	      );
	return 0;
}

int main(int argc, char **argv) {
	int c;
	FILE *fp = NULL;
	uint8_t isfile = 0;
	/* Enable custom error messages, and optional arguments/ */
	opterr = 0;
	/* Get options. */
	while (optind < argc) {
		if ((c = getopt(argc, argv, opts)) != -1) {
			switch(c) {
				case 'c':
					colltype = atoi(optarg);
					break;
				case 'v':
					verbose = atoi(optarg);
					break;
				case 'h':
					usage();
					return 2;
				case '?':
					if (isprint(optopt)) {
						if (optopt == 'c') {
							fprintf(stderr, "Setting colltype to default.\n");
							colltype = -1;
							break;
						} else if (optopt == 'v') {
							fprintf(stderr, "Enabling single level verbosity.\n");
							verbose = 1;
							break;
						} else {
							fprintf(stderr
								, "The option that you have typed in, which in this case\n"
								  "is -%c, does not exist.\n"
								  "Try looking at the help message, or RTFM, next time.\n"
								, optopt);
						}
					} else {
						fprintf(stderr
							, "What the fuck are you trying to type in, a binary blob?\n"
							  "Because of this, clld will exit, so, stop acting like a\n"
							  "jackass.\n");
					}
					return -1;
				default: ;
			}
		} else {
			if (!isfile) {
				fp = fopen(argv[optind], "r");
				if (fp == NULL) {
					fprintf(stderr, "Failed to open file %s\n", optarg);
					return -1;
				}
			}
			isfile = (fp != NULL);
			optind++;
		}
	}

	/* Create the Cursor, and the Floor */
	struct colis cursor;
	struct colis floor;

	/*
	 * Create a temporary Cursor/Floor, because it simplifies
	 * the process of getting collision data, to one for loop.
	 */
	struct colis tmp;
	int hmpeak = 0;
	/* Get collision type. */
	char *buf = NULL;
	size_t size;
	getline(&buf, &size, (isfile) ? fp : stdin);

	if (colltype == -1)
		colltype = atoi(strtok(buf, "\n"));

	for (int coll = 0; coll <= 1; coll++) {
		buf = NULL;
		getline(&buf, &size, (isfile) ? fp : stdin);
		/*
		 * Format is:
		 *
		 * <w>, <h>
		 */
		tmp.w = atoi(strtok(buf, ","));
		tmp.h = atoi(strtok(NULL, "\n"));
		/*
		 * Initialize heightmap.
		 */
		if (colltype == THM && coll)
			tmp.heightmap = malloc(sizeof(uint8_t *)*tmp.w);
		/* Get Tile Heightmap data. */
		if (colltype == THM && coll) {
			/* Get vertical flip flag */
			buf = NULL;
			getline(&buf, &size, (isfile) ? fp : stdin);
			floor.flp = buf[0] - '0';
			/* Get Heightmap */
			buf = NULL;
                        size_t newidth =  getline(&buf, &size, (isfile) ? fp : stdin);

			/*
			 * Format is:
			 *
			 * 0, 1, 2, 3, ... Until element w-1.
			 */
			tmp.w = (newidth >= tmp.w) ? tmp.w : newidth;
			for (unsigned int i = 0; i <tmp.w; i++) {
				char *tmptok = strtok((i == 0) ? buf : NULL,",");
				tmptok = (tmptok == NULL) ? strtok(NULL,"\n") : tmptok;
				tmp.heightmap[i] = (uint8_t)((atoi(tmptok) <= tmp.h) ? atoi(tmptok) : tmp.h);
				hmpeak = (atoi(tmptok)+1 >= hmpeak) ? atoi(tmptok)+1 : hmpeak;
			}

		}

		/*
		 * Copy width, and height to either
		 * the Cursor, or the Floor.
		 */
		(coll) ? (floor.w = tmp.w) : (cursor.w = tmp.w);
		(coll) ? (floor.h = tmp.h) : (cursor.h = tmp.h);
		if (coll)
			floor.heightmap = tmp.heightmap;
		/* Debugging stuff. */
		if (verbose >= 1) {
			if (colltype == THM && coll)
				 for (unsigned int i = 0; i<floor.h; i++)
					 printf((i < floor.h-1 && i % 16 != 0 || i == 0) ? "%u, " : ("%u\n"), floor.heightmap[i]);
		}


		uint8_t delms = 0;
		char *tmp2;
		buf = NULL;
		getline(&buf, &size, (isfile) ? fp : stdin);
		for (int j = 0; buf[j] != '\0'; j++)
			if (buf[j] == ',')
				delms++;
		postype = (delms == 2) ? 1 : 0;
		/* Check for floating point coordinates. */
		uint8_t flt;
		flt = (strchr(buf, '.') != NULL) ? 1 : 0;
		if (flt) {
			tmp.pos.fx = atof(strtok(buf,","));
			if(postype)
				tmp.pos.fy = atof(strtok(NULL,","));
			tmp2 = strtok(NULL, "\n");
			TERASGN(postype, tmp.pos.fz, tmp.pos.fy, atof((tmp2 == NULL ? strtok(NULL,"\0") : tmp2)));
			(coll) ? (floor.pos = tmp.pos) : (cursor.pos = tmp.pos);
			if (verbose >= 1) {
				printf("%07.06f, %07.06f%s", (!coll) ? cursor.pos.fx : floor.pos.fx, (!coll) ? cursor.pos.fy : floor.pos.fy, (postype) ? ", " : "\n");
				if(postype)
                           		printf("%07.06f\n", (!coll) ? cursor.pos.fz : floor.pos.fz);
			}
		} else {
			tmp.pos.x = atoi(strtok(buf,","));
			/* Are we using 3D coordinates, or 2D coordinats? */
			if(postype)
				tmp.pos.y = atoi(strtok(NULL,","));
			tmp2 = strtok(NULL, "\n");
			TERASGN(postype, tmp.pos.z, tmp.pos.y, atoi((tmp2 == NULL ? strtok(NULL,"\0") : tmp2)));
			(coll) ? (floor.pos = tmp.pos) : (cursor.pos = tmp.pos);
			if (verbose >= 1) {
				printf("%i, %i%s", (!coll) ? cursor.pos.x : floor.pos.x, (!coll) ? cursor.pos.y : floor.pos.y, (postype) ? ", " : "\n");
				if(postype)
                           		printf("%i\n", (!coll) ? cursor.pos.z : floor.pos.z);
			}
		}
		/* Get X, and Y velocity. */
		buf = NULL;
		getline(&buf, &size, (isfile) ? fp : stdin);
		if (!coll) {
			cursor.xvel = atof(strtok(buf, ","));
			cursor.yvel = atof(strtok(NULL, "\n"));
		} else {
			floor.xvel = atof(strtok(buf, ","));
			floor.yvel = atof(strtok(NULL, "\n"));
		}

		(!flt) ? (cursor.a1 = cursor.pos.x) : (cursor.c1 = cursor.pos.fx);
		(!flt) ? (cursor.a2 = cursor.pos.x+cursor.w) : (cursor.c2 = cursor.pos.fx+cursor.w);
		(!flt) ? (cursor.b1 = cursor.pos.y) : (cursor.d1 = cursor.pos.fy);
		(!flt) ? (cursor.b2 = cursor.pos.y+cursor.h) : (cursor.d2 = cursor.pos.fy+cursor.h);
		(!flt) ? (floor.a1 = floor.pos.x) : (floor.c1 = floor.pos.fx);
		(!flt) ? (floor.a2 = floor.pos.x+floor.w) : (floor.c2 = floor.pos.fx+floor.w);
		(!flt) ? (floor.b1 = floor.pos.y) : (floor.d1 = floor.pos.fy);
		(!flt) ? (floor.b2 = floor.pos.y+floor.h) : (floor.d2 = floor.pos.fy+floor.h);

		if (coll) {
			switch (colltype) {
				case THM : (!flt) ? thm(hmpeak, &cursor, &floor) : fthm(hmpeak, &cursor, &floor); break;
				case AABB: (!flt) ? aabb(&cursor, &floor       ) : faabb(&cursor, &floor       ); break;
			}
		}
	}
	uint8_t a = 0;
	unsigned int i = 0;
	free(buf);
	if(colltype == THM) {
		free(floor.heightmap);
	}
	fflush(stdout);
	return 0;
}
