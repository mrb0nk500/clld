#!/bin/sh
x1=234
y1=234
xvel=1
xspd=$xvel
yvel=1
yspd=$yvel
xspd2=$xvel
yspd2=$yvel
x2=256
y2=256
flip=0
#heightmap="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1"
heightmap="16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16"
#heightmap="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16"
#heightmap="16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1"
w1=16
w2=64
h1=16
h2=64
u=0
d=0
l=0
r=0
clear
actkbd -Ps -d /dev/input/by-path/platform-i8042-serio-0-event-kbd | \
while read frame key held; do
	yspd=0 && xspd=0 && negx="" && negy="";
	yspd2=0 && xspd2=0 && negx2="" && negy2="";
	if [ "$held" = "1" ]; then
        	[ "${key#*KEY_W}" != "$key" -o "${key#*KEY_S}" != "$key" ] && yspd=$yvel
        	[ "${key#*KEY_A}" != "$key" -o "${key#*KEY_D}" != "$key" ] && xspd=$xvel
        	[ "${key#*KEY_W}" != "$key" ] && negy="-" && y1=$(echo "$y1-$yspd" | bc)
        	[ "${key#*KEY_S}" != "$key" ] && negy=""  && y1=$(echo "$y1+$yspd" | bc)
        	[ "${key#*KEY_A}" != "$key" ] && negx="-" && x1=$(echo "$x1-$xspd" | bc)
        	[ "${key#*KEY_D}" != "$key" ] && negx=""  && x1=$(echo "$x1+$xspd" | bc)
	fi

	str=$(printf "0\n$w1,$h1\n$x1,$y1\n$negx$xspd,$negy$yspd\n$w2,$h2\n$flip\n$heightmap\n$x2,$y2"| ./clld)
	cursor=$(printf "$str" | head -n5)
	floor=$(printf "$str" | tail -n5)
	xy=$(printf "$cursor" | tail -n2 | tr -d ',' | head -n1)
	xy2=$(printf "$floor" | tail -n2 | tr -d ',' | head -n1)
	x1=$(echo "$xy" | awk '{print $1}')
	y1=$(echo "$xy" | awk '{print $2}')
	x2=$(echo "$xy2" | awk '{print $1}')
	y2=$(echo "$xy2" | awk '{print $2}')
	printf "$cursor\n$floor"
	#printf "\33[1;1H\33[2K$x1 $y1\33[E\33[2K$negx$xspd $negy$yspd\n\33[2K\n"
	#printf "\33[2K$x2 $y2\33[E\33[2K$negx2$xspd2 $negy2$yspd2\n\33[2K\n"
	if [ "$held" = "1" -a "${key#*KEY_KP}" != "$key" ]; then
        	[ "${key#*KEY_KP8}" != "$key" -o "${key#*KEY_KP2}" != "$key" ] && yspd2=$yvel
        	[ "${key#*KEY_KP4}" != "$key" -o "${key#*KEY_KP6}" != "$key" ] && xspd2=$xvel
        	[ "${key#*KEY_KP8}" != "$key" ] && negy2="-" && y2=$(echo "$y2-$yspd2" | bc)
        	[ "${key#*KEY_KP6}" != "$key" ] && negy2=""  && y2=$(echo "$y2+$yspd2" | bc)
        	[ "${key#*KEY_KP4}" != "$key" ] && negx2="-" && x2=$(echo "$x2-$xspd2" | bc)
        	[ "${key#*KEY_KP6}" != "$key" ] && negx2=""  && x2=$(echo "$x2+$xspd2" | bc)
	fi

done
