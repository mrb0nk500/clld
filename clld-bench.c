/*
 * Main Program file.
 *
 * Platform: 	Anything with a C compiler.
 * Author:	mr b0nk 500 <b0nk@mrb0nk500.xyz>
 * Notes: 	Who would even use this kind of collision checker?
 * Created:	September 27, 2019 @ 09:25PM
 */

#include "clld.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
	int colltype = -1;
	/* Create the Cursor, and the Floor */
	struct colis cursor;
	struct colis floor;
	colltype = THM;
	cursor.h = 16;
	cursor.w = 16;
	cursor.pos.x = 17;
	cursor.pos.y = 24;
	cursor.xvel = 1;
	cursor.yvel = 0;
	floor.w = 16;
	floor.h = 16;
	floor.flp = 0;
	int hmpeak;
	if (colltype == THM) {
		floor.heightmap = malloc(sizeof(uint8_t *)*floor.w);
		int i = 0;
		for (; i < floor.w; i++) floor.heightmap[i] = i+1;
		hmpeak = floor.heightmap[floor.w-1];
	}
	uint8_t flt = 0;
	floor.pos.x = 32;
	floor.pos.y = 32;
	floor.xvel = 0;
	floor.yvel = 0;
	if (!flt) {
		cursor.a1 = cursor.pos.x;
		cursor.a2 = cursor.pos.x+cursor.w;
		cursor.b1 = cursor.pos.y;
		cursor.b2 = cursor.pos.y+cursor.h;
		floor.a1  = floor.pos.x;
		floor.a2  = floor.pos.x+floor.w;
		floor.b1  = floor.pos.y;
		floor.b2  = floor.pos.y+floor.h;
	} else {
		cursor.c1 = cursor.pos.fx;
		cursor.c2 = cursor.pos.fx+cursor.w;
		cursor.d1 = cursor.pos.fy;
		cursor.d2 = cursor.pos.fy+cursor.h;
		floor.c1  = floor.pos.fx;
		floor.c2  = floor.pos.fx+floor.w;
		floor.d1  = floor.pos.fy;
		floor.d2  = floor.pos.fy+floor.h;
	}
	switch (colltype) {
		case THM : (!flt) ? thm(hmpeak, &cursor, &floor) : fthm(hmpeak, &cursor, &floor); break; /* Tile Heightmap Collision. */
		case AABB: (!flt) ? aabb(&cursor, &floor       ) : faabb(&cursor, &floor       ); break; /* Axis Aligned Bounding Box Collision. */
	}
	/*uint8_t a = 0;
	unsigned int i = 0;
	free(buf);*/
	if (colltype == THM) {
		free(floor.heightmap);
	}
	fflush(stdout);

	return 0;
}
