PREFIX := /usr/local
BIN_DIR := $(PREFIX)/bin

ifdef DEBUG
DEBUG_CFLAGS=-DDEBUG
else
DEBUG_CFLAGS=
endif

ifdef PCC
PCC_CFLAGS=-D__float128="long double"
else
PCC_CFLAGS=
endif

CFLAGS = $(CFLAGS_EXTRA) $(PCC_CFLAGS) $(DEBUG_CFLAGS)
OBJS = clld.o
all : clean $(OBJS)

%.o : %.c
	$(CC) -c $< -o $@ $(CFLAGS)
test : all clld-test.o
	$(CC) clld.o clld-test.o -o clld $(CFLAGS)
test-mvmt : clean test
	$(CC) test-mvmt.c -o test-mvmt $(CFLAGS)
benchmark : all clld-bench.o
	$(CC) clld.o clld-bench.o -o clld-bench $(CFLAGS)
clean :
	rm -f clld test-mvmt clld-bench *.o
install :
	install -D -m755 clld $(BIN_DIR)/clld
uninstall :
	rm -f $(BIN_DIR)/clld
