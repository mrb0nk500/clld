#include <stdint.h>

enum {THM, AABB};

struct pos {
	int x;
	int y;
	int z;
	double fx;
	double fy;
	double fz;
};

struct colis {
	/* Cursor/Floor's width, and height */
	int w;
	int h;

	/* Cursor's X, and Y velocity. */
	double xvel;
	double yvel;

	/* Cursor/Floor's coordinates. */
	struct pos pos;

	/* The four corners of either the Cursor, or the Floor. */
	int a1;
	int a2;
	int b1;
	int b2;

	/* The four floating point corners of either the Cursor, or the Floor. */
	double c1;
	double c2;
	double d1;
	double d2;

	/* Heightmap, used in Tile Heightmap collision. */
	uint8_t *heightmap;

	/* X axis collision. */
	unsigned int clx : 1;

	/* Y axis collision. */
	unsigned int cly : 1;

	/* Cursor is on the ground. */
	unsigned int gnded : 1;

	/* Vertical flip flag. */
	unsigned int flp : 1;
};

/* Tile Heightmap Collision. */
extern void thm(int hmpeak, struct colis *cursor, struct colis *floor);
/* Floating point Tile Heightmap Collision. */
extern void fthm(int hmpeak, struct colis *cursor, struct colis *floor);
/* Axis Aligned Bounding Box Collision. */
extern void aabb(struct colis *cursor, struct colis *floor);
/* Floating point Axis Aligned Bounding Box Collision. */
extern void faabb(struct colis *cursor, struct colis *floor);
